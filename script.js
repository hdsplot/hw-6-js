function createNewUser() {
  let newUserFirstName = prompt("Please, type your name");
  let newUserLastName = prompt("Please, type your lastname");
  let newUserBirthday = prompt("Please, type date of your birth(dd.mm.yyyy)");
  let newUser = {
    firstName: newUserFirstName,
    lastName: newUserLastName,
    birthday: newUserBirthday,
    getLogin: function () {
      return this.firstName[0].toLowerCase() + this.lastName.toLowerCase();
    },
    getAge: function () {
      let now = new Date();
      let currentYear = now.getFullYear();

      let inputDate = +this.birthday.substring(0, 2);
      let inputMonth = +this.birthday.substring(3, 5);
      let inputYear = +this.birthday.substring(6, 10);

      let birthDate = new Date(inputYear, inputMonth - 1, inputDate);
      let birthYear = birthDate.getFullYear();
      let age = currentYear - birthYear;
      if (now < new Date(birthDate.setFullYear(currentYear))) {
        age = age - 1
      }
      return age;
    },
    getPassword: function () {
      return this.firstName[0].toUpperCase() + this.lastName.toLowerCase() + this.birthday.substring(6, 10);
    },
  }
 
  return newUser;
}
let user = createNewUser();
console.log(user);
console.log(user.getAge());
console.log(user.getPassword());